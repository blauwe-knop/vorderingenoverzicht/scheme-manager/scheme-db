CREATE SCHEMA scheme;

CREATE TABLE scheme.organization(
    oin varchar(20) NOT NULL,
    name varchar(255) NOT NULL,
    discovery_url varchar(2048) NOT NULL,
    public_key text NOT NULL,
    approved boolean NOT NULL,
    logo_url text NOT NULL,
    PRIMARY KEY (oin)
);
