# financial-claim-request-db Helm chart

By default, this chart uses a cloud-native postgresql setup. To use an external database instead, 
simply set `pgCloudNativeEnabled: false`.

## Postgresql secret
The cloud-native postgresql setup creates a secret like the following:
```
Name:         scheme-db-app
Namespace:    bk-scheme
Labels:       cnpg.io/cluster=scheme-db
              cnpg.io/reload=true
Annotations:  cnpg.io/operatorVersion: 1.22.0

Type:  kubernetes.io/basic-auth

Data
====
jdbc-uri:  134 bytes
pgpass:    103 bytes
host:      12 bytes
password:  64 bytes
port:      4 bytes
uri:       115 bytes
user:      9 bytes
username:  9 bytes
dbname:    9 bytes
```
When disabling cloud-native postgresql, be sure to manually create a secret like above, 
because "username" and "password" are still being used.