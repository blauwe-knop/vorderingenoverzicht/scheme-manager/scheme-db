#!/bin/bash
set -uo pipefail

# TODO: Don't use PGPASSWORD as commandline argument
migrate --database "postgres://${PGUSER}:${PGPASSWORD}@${PGHOST}:${PGPORT}/${PGDATABASE}?connect_timeout=5&sslmode=${PGSSLMODE}" --lock-timeout 600 --path /migrations/ up
retVal=$?;
if [ $retVal -ne 0 ]
then
    # Sleep for a while to prevent a lot
    # of restarts when postgres is not ready
    echo "Migrations failed, sleeping before exiting"
    sleep 10 
    exit 1
fi
echo "Migrations command completed successfully"
